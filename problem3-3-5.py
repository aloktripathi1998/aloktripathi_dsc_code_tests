

import sys
import csv
import operator
import requests

if len(sys.argv) < 2:
     print("Please pass path for output file")
     sys.exit(0)

print("Starting to process input file from https://dsc-data-challenge.s3-us-west-2.amazonaws.com/pageViews.csv")

CSV_URL = "https://dsc-data-challenge.s3-us-west-2.amazonaws.com/pageViews.csv"

with requests.Session() as s:
    download = s.get(CSV_URL)
    decoded_content = download.content.decode('utf-8')
    cr = csv.reader(decoded_content.splitlines(), delimiter=',')

data = sorted(cr,key=operator.itemgetter(1))

# initialize dictionary<String, Array> to store user operations
lookupTable = {'user': ['clicks']}
print("Successfully read file, processing...")

# go through each line in file and pre process it.
for line in data:
    # If user click is encountered before, append the action, else initialize its array.
    if line[2] in lookupTable.keys():
        x = lookupTable[line[2]]
        x.append(line[0])
        #print(line)
    else:
        lookupTable[line[2]] = []

print("done pre processing file, preparing data structure to write file.")

for line in data:
    # check if entry for user is present in lookup table.
    if line[2] in lookupTable.keys():
        # get the list of event_ids for a user
        x = lookupTable[line[2]]
        if len(x) > 0:
            # append next event_id to current line.
            line.append(x[0])
            # remove this entry from lookup table
            x.pop(0)

#for eachline in data:
#    print(eachline)

print("Starting to write to file", sys.argv[1])
with open("output_python_3_5.csv", "w", newline='') as f:
    fileWriter = csv.writer(f, delimiter=',')
    fileWriter.writerow(['event_id', 'time','user_id', 'path','next_event_id'])
    for row in data:
        fileWriter.writerow(row)
print("**Done**")