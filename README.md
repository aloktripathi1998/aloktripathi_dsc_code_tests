# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

For Problem 3 [finding the next event_id in time order for each user]

I am giving 3 different solutions:

1. Through Python (using urllib library and Dict and Lists data structure. one solution is for python 2.6 and another one is for python 3.5) 
2. Spark SQL, data frames, Databricks cluster and databricks notebook utility
3. Plain Analytical SQL

### How do I get set up? ###
-----------------------------------SOLUTION 1 (1.1 and 1.2) ----------------------------------------------
1. You just need python 2.6 or 3.5 to run the programs. You should be able to run it through command line.
Run this in linux command line : 

1.1
for python 2.6 
python problem3-2-6.py <outputfilepath>                

### This will generate output file named "Output_2_6.csv" at given location###

1.2 for python 3.5 
python problem3-3-5.py <outputfilepath>

### This will generate output file named "Output_python_3_5.csv" at given location###


-----------------------------------SOLUTION 2 (pyspark on databricks)---------------------------------------
2. Because of the time contraint, I used databricks spark solutions and notebooks to write pyspark code.
 steps: 1. Need to download the file into DBFS.
 		2. The code will generate output_pageviews.csv file in DBFS system. Please see the snapshot attached in word doc.

-----------------------------------SOLUTION 3 (Analytical SQL)------------------------------------------------
3. Assuming the csv file is in a DB table (a DB supporting the analytical function). SQL will give the desired output.

