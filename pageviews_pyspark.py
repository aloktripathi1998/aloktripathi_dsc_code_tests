import sys
from pyspark.sql.functions import rank,lead
from pyspark.sql import Window

#Read the csv and create a dataframe
df_csv = spark \
    .read \
    .format("com.databricks.spark.csv") \
    .option("header", "true") \
    .load("/FileStore/tables/70m1fvmv1501635042050/pageViews.csv")  #Pointing to the file in DBFS
    #One may need to change the path for the csv to run this code. 
    #Ideally, in enterprise applications this will loading data from HDFS or S3.

#perform partition by domain_userid and sort timestamp   
w=Window.partitionBy("domain_userid").orderBy("collector_tstamp")

#Take the lead (next event_id) for each line item over w
df_next_eventid=df_csv.withColumn("next_event_id",lead("event_id").over(w))

#register as a temp table (if you want to query)
df_next_eventid.registerTempTable("table_PageViews")

#write the temptable into csv
spark.table("table_PageViews").write.option("delimiter", ",").csv("/FileStore/output_pageviews.csv")

#To Delete the file in DBFS from Databricks notebook
#%fs rm -r FileStore/output_pageviews.csv

#To see the file in DBFS from Databricks notebook
#%fs ls FileStore/output_pageviews.csv