


--select * from pageViews

select 
event_id
,collector_tstamp
,domain_userid
,page_urlpath
,lead(event_id) over(partition by domain_userid order by collector_tstamp asc) as next_event_id
from pageViews
order by domain_userid,collector_tstamp


